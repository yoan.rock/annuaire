#! /bin/bash

function verifFile() { 
if [ -e $1 ]; then
	a=1
else
	echo "not existing file"
	return 0
fi

if [ -r $1 ]; then
	a=1
else
	echo "file not writable"
	return 0
fi

if [ -w $1 ]; then
	a=1
else
	echo "file not readable"
	return 0
fi
}



#Exo Annuaire



if [ "$#" -eq 2 ]; then
	FILENAME=$2

elif [ "$#" -eq 3 ]; then
	FILENAME=$3

elif [ "$#" -eq 1 ]; then
	FILENAME=$1
	sort $FILENAME
else 
	echo "./annuaire <filename> <arg>"
	return 0
fi




verifFile $FILENAME

while getopts "tTMdmg:a:es:b::" option; do

	case "${option}" in

		t)
sort -t: +4 +1 $FILENAME ;;

T)
sort -t: +2n $FILENAME ;;

M)
cut -d: -f1 $FILENAME ;;
d)
tail -n 1 $FILENAME
echo -e '\n';;
m)

while IFS=':' read -r nom prenom numero bureau service
do
	echo "$(echo "$nom" | sed 's/.*/\u&/').$(echo "$prenom" | sed 's/.*/\u&/')"
done <$FILENAME
;;

g)
grep -i $OPTARG $FILENAME ;;
a)
echo "nom:"
read nom
echo "prenom:"
read prenom
echo "num_tel:"
read num_tel
echo "bureau:"
read bureau
echo "service:"
read service

echo -e "\n"$nom":"$prenom":"$num_tel":"$bureau":"$service
echo "Confirmez-vous ? (o/n)"
read reponse
case "$reponse" in
o|oui)	
echo -e "\n"$nom":"$prenom":"$num_tel":"$bureau":"$service >> $FILENAME
echo $nom" enregistré";;
	
n|non)	echo "non enregistré";;
esac
;;

e)
mkdir service
for f in $(cut -d: -f5 $FILENAME | sort -u)
do
grep $f $FILENAME >> ./service/$f
done;;
s)
sed -i "/$OPTARG/d" $FILENAME
echo $OPTARG" deleted";;
b)

grep "^[^:]*:[^:]*:[^:]*:$2.*:" $FILENAME | cut -d: -f1|sort;;

esac

done


